import pymumble.pymumble_py3 as pymumble_py3
from pymumble.pymumble_py3.constants import PYMUMBLE_CONN_STATE_NOT_CONNECTED
import subprocess
import datetime
import yaml
import inotify.adapters
import inotify.constants
import time


def get_config():
    with open("config.yml", 'r') as configfile:
        return yaml.safe_load(configfile)


def execute_command(command, wait=False):
    command = f'script -q -B /dev/null -c "{command}"'
    process = subprocess.Popen(command, shell=True)
    if wait:
        process.communicate()


def spawn_bot(bot, wait=False):
    # prevent a bot to spawn multiple times
    kill_bot(bot, wait=True)
    execute_command(bot['launcher'], wait=wait)


def kill_bot(bot, wait=False):
        execute_command(bot['stopper'], wait=wait)



def check_bots():
    global bot_count
    try:
        config = get_config()
    except Exception as exc:
        print(exc)
    try:
        server = config['server']
        passwd = config['passwd']
        nick = config['nick']
        BotList = config['BotList']
    except KeyError as e:
        print(e)

    mumble = pymumble_py3.Mumble(server, nick, password=passwd)
    mumble.start()
    # the mumble.is_ready() have bug which prevent to use it fr waiting the connection to establish, for now we will just wait 0.5 second
    time.sleep(0.5)
    if not mumble.is_ready():
        users = []
        for user in mumble.users:
            users.append(mumble.users[user]['name'])
        for botname in BotList:
            bot = BotList[botname]
            if bot.get("masters") is None:
                if len(users) > bot_count + 1 and not (botname in users):
                    spawn_bot(bot)
                    bot_count += 1
                elif botname in users and len(users) == bot_count + 1:
                    kill_bot(bot)
                    bot_count -= 1
            if bot.get("masters") is not None:
                if not botname in users and any([u in users for u in bot["masters"]]):
                    spawn_bot(bot)
                    bot_count += 1
                if botname in users and not any([u in users for u in bot["masters"]]):
                    kill_bot(bot)
                    bot_count -= 1
    # Disconnect MumbleWatche Bot
    mumble.reconnect = False
    mumble.connected = PYMUMBLE_CONN_STATE_NOT_CONNECTED
    mumble.control_socket.close()


def main():
    config = get_config()
    murmur_db = config['murmurDB']
    # Because we use a bot count, we must disconnect all bots before
    for bot in config["BotList"]:
        kill_bot(config["BotList"][bot])
    # watch on murmur.sqlite change
    i = inotify.adapters.Inotify()
    i.add_watch(murmur_db, mask=inotify.constants.IN_MODIFY)
    # bot invocation date
    last_spawn = datetime.datetime.now()
    # launch all bots if necessary
    check_bots()
    for event in i.event_gen(yield_nones=False):
        time_elapsed = datetime.datetime.now() - last_spawn
        # Forbid the bot to detect himself and prevent infinite spawn
        if time_elapsed.total_seconds() > 0.5:
            check_bots()
            last_spawn = datetime.datetime.now()


bot_count = 0
main()
