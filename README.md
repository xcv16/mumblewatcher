# MumbleWatch

This is an attempt to write a watcher for mumble for autospawning/autokilling bot depending on many people are connected in a mumble server.

For now it's just watching any access on the sqlite file of murmur database. It's using the awesome [pymumble](https://github.com/azlux/pymumble) for managing all aspect of connecting to a mumble server.

When starting, it will kill ALL OTHERs bots defined in config.yml

## Installation
### Dependencies
```shell
git submodule init;git submodule update
pip install -r requirements.txt
```
### Configuration
The user executing this script must have access to the **murmur.sqlite database**
Copy-Paste **config.yaml.sample** in **config.yml** and set all fields:
```yaml
server: The mumble server URL
nick: This bot username (it will appear in mumble chat)
murmurDB: The path to the murmur.sqlite database
passwd: The passaword of the mumble server

BotList: A YAML list listing all bots
    Bot1: Bot1 username
        masters: OPTIONAL, if present the bot will spawn only if one username is on this list
            - master1
            - master2
        launcher: BASH command for starting the bot
        stopper: BASH command for stopping the bot
```

## Why ?

I was bored during the COVID-19 "social distanciation" and tired of always seeing my mumble server reporting connected users when only bots were present.
